<!DOCTYPE html>
<%-- Directiva de JSPs  --%>
<%@page import="org.hibernate.Session"%>
<%@page import="talent.campus.utils.HibernateUtils"%>

<%@page import="org.hibernate.query.Query"%>

<%@ page import="java.util.Random"%>
<%@ page contentType="text/html"%>
<html>
<body>

	<%-- tag para declarar una variable en el servlet que se generar� --%>
	<%!Integer randomVal;%>

	<%-- JSP Expression: se evalua   --%>
	<h2>
		Welcome IP:
		<%=request.getRemoteAddr()%>
	</h2>

	<%
		//primer scriptlet
		Random demo = new Random();
		this.randomVal = Integer.valueOf(demo.nextInt());
		out.println("Random integer: " + this.randomVal);		
	%>

	<%
		if (this.randomVal % 2 == 0) {
	%>
	<p>Par</p>
	<%
		} else {
	%>
	<p>Impar</p>
	<%
		}
	%>


	<%
		for (int fontSize = 1; fontSize <= 3; fontSize++) {
	%>
	<font color="green" size="<%=fontSize%>"> JSP Tutorial </font>
	<br />
	<%
		}
	%>
	
	<%
	Session session1 = HibernateUtils.getSessionFactory().openSession();
	
	Query query = session1.createQuery("SELECT u FROM Usuario u");
	query.list();
	
	%>
</body>
</html>
