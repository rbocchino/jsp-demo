<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="talent.campus.Profesor"%>

<!DOCTYPE html>
<html>

<head>
<title>Using JavaBeans in JSP</title>
</head>

<body>
	<h2>Using JavaBeans in JSP</h2>
	<jsp:useBean id="test" class="talent.campus.MessageBean"
		scope="session" />
	<jsp:setProperty name="test" property="message" value="Hello JSP..." />

	<p>Got message....</p>
	<jsp:getProperty name="test" property="message" />

	<%
		ArrayList profesores = new ArrayList();
		profesores.add(new Profesor("Profe1"));
		profesores.add(new Profesor("Profe2"));
		profesores.add(new Profesor("Profe3"));
		request.setAttribute("profesores", profesores);
	%>

	<p>
		<c:forEach var="i" begin="1" end="5">
         Item <c:out value="${i}" />
			<p>
		</c:forEach>
	<form id="form-1">
		<select id="cars" name="cars">


			<c:forEach var="profesor" items="${profesores}">
				<option value="${profesor.name}">${profesor.name}</option>
			</c:forEach>
		</select>
	</form>


</body>
</html>