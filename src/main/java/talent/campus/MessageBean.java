package talent.campus;

public class MessageBean {
	private String message = "No message specified";

	public String getMessage() {
		return (this.message);
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
